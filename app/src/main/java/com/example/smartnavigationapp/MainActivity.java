package com.example.smartnavigationapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.text.TextUtils.isEmpty;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //defining view objects
    private EditText username;
    private EditText emcontact;
    private EditText eemail;
    private EditText epassword;
    private Button signup;
    private ProgressDialog progressDialog;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;


    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        //initializing views
        username =  (EditText)findViewById(R.id.uname);
        emcontact=(EditText)findViewById(R.id.econtact);
        eemail = (EditText) findViewById(R.id.email);
        epassword = (EditText) findViewById(R.id.password);

        signup = (Button) findViewById(R.id.signup);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        signup.setOnClickListener(this);

        databaseReference=FirebaseDatabase.getInstance().getReference("User");
    }

    private void registerUser(){

        //getting email and password from edit texts
        final String user=username.getText().toString().trim();
        final String contact=emcontact.getText().toString().trim();
        final String email = eemail.getText().toString().trim();
        final String password = epassword.getText().toString().trim();



        if(isEmpty(user)){
            Toast.makeText(this,"Please enter username",Toast.LENGTH_LONG).show();
            return;
        }


        if(isEmpty(contact)){
            Toast.makeText(this,"Please enter emergency contact no",Toast.LENGTH_LONG).show();
            return;
        }

        if(isEmpty(user)){
            Toast.makeText(this,"Please enter username",Toast.LENGTH_LONG).show();
            return;
        }
        //checking if email and passwords are empty
        if(isEmpty(email)){
            Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
            return;
        }

        if(isEmpty(password)){
            Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if(task.isSuccessful()){

                            User information=new User(
                                    user,contact,email,password

                            );

                            FirebaseDatabase.getInstance().getReference("User")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(information).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    //display some message here
                                    Toast.makeText(MainActivity.this,"Successfully registered",Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(getApplicationContext(),Login.class));
                                }
                            });


                        }else{
                            //display some message here
                            Toast.makeText(MainActivity.this,"Registration Error",Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    @Override
    public void onClick(View view) {
        //calling register method on click
        registerUser();
    }
}