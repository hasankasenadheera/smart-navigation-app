package com.example.smartnavigationapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splashscreen extends AppCompatActivity {

    public static int SPLASH_TIME_OUT=9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent MainActivity=new Intent(Splashscreen.this, com.example.smartnavigationapp.Login.class);
                startActivity(MainActivity);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
}
